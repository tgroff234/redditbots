import praw
import time
import re
import signal
import pickle
import sys

done = []

with open("bot.pickle", "rb") as f:
	try:
		done = pickle.load(f)
	except EOFError:
		done = []

r = praw.Reddit("bot for following and measuring *-a-roo rabbit holes\
				by /u/thecakeizalie")

r.login()

def sighandler(signal, frame):
	print("dumping bot state")
	with open("bot.pickle", "wb") as f:
		pickle.dump(done, f)

signal.signal(signal.SIGINT, sighandler)

followed = []
counter = 1

def follow(link):
	
	global followed
	global counter
	counter += 1
	print("%d layers deep"%counter)
	
	submission = r.get_submission(link)
	print('aquired submission')

	bottom = True

	for comment in praw.helpers.flatten_tree(submission.comments):
		searchable = re.sub(r"[\[\]]|\(.*\)", "", comment.body)

		if re.search("([tT]he|[yY]e) (good )?'?[oO]l['de]?e?'? ?([rR]eddit)?", searchable):
			print(searchable.replace(u'\u2026', ''))
			bottom = False
			if comment.id in followed:
				return "rabbit hole is %d links deep and ends in a loop" % counter
				
			print('adding comment to followed list')	
			followed.append(comment.id)

			print('following')
			print(comment.body.replace(u"\u2026", ""))
			l = comment.body.split("(")[1].split(")")[0]
			return follow(l)
		if type(comment) == praw.objects.MoreComments:
			break
	if bottom:
		print('bottom of the hole')
		return "rabbit hole is %d links deep"%counter 
	#except AttributeError:
	#	print('attribute error occured')
#try:
while True:
	print('>getting submissions')
	sub = r.get_subreddit("switcharoo")
	#for submission in sub.get_hot(limit=100):
	submission = r.get_submission('http://www.reddit.com/r/funny/comments/2wjyf0/show_us_ur_keyboard/corvxkb?context=3')
	comments = praw.helpers.flatten_tree(submission.comments)
	try:
		print('>  %d comments in %s'%(len(comments), submission.title))
	except UnicodeEncodeError:
		pass

	for comment in comments:
		
		if type(comment) == praw.objects.MoreComments:
			continue
		searchable = ""
		if re.search("\[.*\]\(.*\)", comment.body):
			searchable = re.sub(r"[\[\]]|\(.*\)", "", comment.body)
		if re.search("([tT]he|[yY]e) (good )?'?[oO]l['de]?e?'? ?([rR]eddit)?.*oo", searchable):
			link = comment.body.split("(")[1].split(")")[0]	
			if "reddit.com" in link:
				counter = 1
				followed = []
				followed.append(comment.id)
				print("hold my beer... I'm going in.")
				print(follow(link))

#except:
#	print("dumping bot state")
#	with open("bot.pickle", "wb") as f:
#		pickle.dump(done, f)